#![forbid(unsafe_code)]

extern crate chrono;
extern crate clap;
extern crate serde_derive;

use std::{
    collections::HashMap,
    io::{BufRead, BufReader, Write},
    net::{IpAddr, Ipv4Addr, Shutdown, SocketAddr, TcpListener, TcpStream},
    str,
};

use chrono::Local;
use clap::Parser;
use serde_derive::{Deserialize, Serialize};
use serde_json::{json, Result};

#[derive(Parser, Default, Debug)]
struct Args {
    a: u8,
    b: u8,
    c: u8,
    d: u8,
    port: u16,
}

#[derive(Deserialize)]
struct RequestStore {
    #[allow(dead_code)]
    request_type: String,
    key: String,
    hash: String,
}

#[derive(Deserialize)]
struct RequestLoad {
    #[allow(dead_code)]
    request_type: String,
    key: String,
}

#[derive(Deserialize, Serialize, Debug, Default)]
struct SuccessLoadResponse {
    response_status: String,
    requested_key: String,
    requested_hash: String,
}

impl SuccessLoadResponse {
    fn new() -> Self {
        Self {
            response_status: String::new(),
            requested_key: String::new(),
            requested_hash: String::new(),
        }
    }
}

fn do_store_request(
    mut stream: &TcpStream,
    request: &RequestStore,
    storage: &mut HashMap<String, String>,
    other_data: &(SocketAddr, String),
) {
    let success_response = json!({"response_status": "success"}).to_string();
    let (new_key, new_hash) = (request.key.clone(), request.hash.clone());
    storage.insert(new_key, new_hash);
    println!(
        "[{:?}] [{:?}] Received request to write new value {:?} by key {:?}. Storage size: {:?}.",
        other_data.0,
        other_data.1,
        request.hash,
        request.key,
        storage.len()
    );
    stream.write_all(success_response.as_bytes()).ok();
}

fn do_load_request(
    mut stream: &TcpStream,
    request: &RequestLoad,
    storage: &mut HashMap<String, String>,
    other_data: &(SocketAddr, String),
) {
    let key_error_response = json!({"response_status": "key not found"}).to_string();
    println!(
        "[{:?}] [{:?}] Received request to get value by key {:?}. Storage size: {:?}.",
        other_data.0,
        other_data.1,
        request.key,
        storage.len()
    );
    let got_value = storage.get(&request.key);
    if let Some(value) = got_value {
        let mut response: SuccessLoadResponse = SuccessLoadResponse::new();
        response.response_status = "success".to_string();
        response.requested_key = request.key.clone();
        response.requested_hash = value.to_string();
        let success_load_response = serde_json::to_string(&response).unwrap();
        stream.write_all(success_load_response.as_bytes()).ok();
    } else {
        stream.write_all(key_error_response.as_bytes()).ok();
    }
}

fn handle_connection(mut stream: &TcpStream, storage: &mut HashMap<String, String>) -> Result<()> {
    let date = Local::now().format("%Y-%m-%d:%H:%M:%S").to_string();
    let mut buff: Vec<u8> = vec![];
    let mut buf_reader = BufReader::new(stream);
    let client_address_res = stream.peer_addr();
    if client_address_res.is_err() {
        println!("There is something wrong with your stream");
        stream.shutdown(Shutdown::Both).ok();
        return Ok(());
    }
    let read_res = buf_reader.read_until(b'}', &mut buff);
    if read_res.is_err() {
        println!("Please, use JSON format for your requests");
        return Ok(());
    }
    let client_address = client_address_res.unwrap();
    let stream_json_res = str::from_utf8(&buff);
    if stream_json_res.is_err() {
        println!("Please, use JSON format for your requests");
        return Ok(());
    }
    let stream_json = stream_json_res.unwrap();
    let request_res: Result<RequestStore> = serde_json::from_str(stream_json);
    if let Ok(request) = request_res {
        let other_data = (client_address, date);
        do_store_request(stream, &request, storage, &other_data);
    } else {
        let request_res: Result<RequestLoad> = serde_json::from_str(stream_json);
        if let Ok(request) = request_res {
            let other_data = (client_address, date);
            do_load_request(stream, &request, storage, &other_data);
        } else {
            stream
                .write_all(
                    "Wrong type of request, only possible request types are:\nload, store.\n"
                        .as_bytes(),
                )
                .ok();
        }
    }
    Ok(())
}

pub fn run(ip: IpAddr, port: u16) -> Result<()> {
    let socket = SocketAddr::new(ip, port);
    let listener = TcpListener::bind(socket).unwrap();
    let mut storage: HashMap<String, String> = HashMap::new();
    let greeting = json!({"student_name": "Roman Asadullin"}).to_string();
    for stream in listener.incoming() {
        if let Ok(mut stream) = stream {
            let date = Local::now().format("%Y-%m-%d:%H:%M:%S").to_string();
            let client_address = stream.peer_addr().unwrap();
            println!(
                "[{:?}] [{:?}] Connection established. Storage size: {:?}.",
                client_address,
                date,
                storage.len()
            );
            stream.write_all(greeting.as_bytes()).ok();
            handle_connection(&stream, &mut storage)?;
        } else {
            continue;
        }
    }
    Ok(())
}

fn main() {
    let args = Args::parse();
    let (a, b, c, d, port) = (args.a, args.b, args.c, args.d, args.port);
    let server_ip: IpAddr = IpAddr::V4(Ipv4Addr::new(a, b, c, d));
    run(server_ip, port).ok();
}
